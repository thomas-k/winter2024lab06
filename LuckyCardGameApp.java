import java.util.*;

public class LuckyCardGameApp {
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		
		Deck cardDeck = new Deck();
		cardDeck.shuffle();
		
		System.out.println("Welcome to the lucky card game! How many cards do you want to remove from the deck?");
		int input = reader.nextInt();
		reader.nextLine();
		while(input < 0 || input > 52){
			System.out.println("Invalid amount. Please input a number between 0 and 52.");
			input = reader.nextInt();
			reader.nextLine();
		}
		
		System.out.println("Initial number of cards: " + cardDeck.numberOfCards);
		for(int i = 0; i < input; i++){
			cardDeck.drawTopCard();
		}
		System.out.println("Amount of cards after removal: " + cardDeck.numberOfCards);
		
		cardDeck.shuffle();
		System.out.println(cardDeck);
	}
}